import source
def get_site(url):
    return url.split('/')[2]

for item in source.source:
    links =  item['text'].split('\n')
    title = links[0]
    tags = title.replace(' ','').split('#')[1:]
    links = [i for i in links if i][1:]
    z = zip(links[::2],links[1::2])
    for title,url in z:
        content=source.template.format(title,url,get_site(url),str(tags))
        with open('../content/items/{}.md'.format(get_site(url).replace('.','-')),'w',encoding='UTF-8') as f:
            f.write(content)
    print(links)





